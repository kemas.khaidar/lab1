from django.shortcuts import render
from datetime import datetime

# Enter your name here
mhs_name = 'Kemas Khaidar Ali' # TODO Implement this
mhs_born = 1999
mhs_month = 5
mhs_day = 1

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(mhs_born, mhs_month, mhs_day)}
    return render(request, 'index.html', response)

# TODO Implement this to complete last checklist
def calculate_age(birth_year, birth_month, birth_day):
    year = datetime.now().year
    month = datetime.now().month
    day = datetime.now().day
    
    tahun = 0
    
    if (month < birth_month):
        tahun = year - birth_year - 1
    else if (month == birth_month):
        if(day < birth_day):
            tahun = year - birth_year - 1
        else:
            tahun = year - birth_year
    else:
        tahun = year - birth_year
    
    
    return tahun
